import ReactDOM from 'react-dom';
import App from './App';
// import { add } from './module1';

// const result = add(3, 5);
// console.info(result);

const app = document.getElementById('app');
ReactDOM.render(<App />, app);
