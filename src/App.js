import React, { useState, useEffect } from 'react';

const App = () => {
  const [persons, setPersons] = useState([]);
  const [personsNummber, setPersonsNumber] = useState(0);
  const [personsLoaded, setPersonsLoaded] = useState(false);
  useEffect(() => {
    fetch('http://localhost:8080/person')
      .then((res) => res.json())
      .then((data) => {
        // console.log(data);
        setPersonsLoaded(true);
        setPersons((prs) => [...prs, ...data]);
        setPersonsNumber(data.length);
      });
  }, []);
  return (
    <div>
      <h2>Persons</h2>
      <p>{personsNummber}</p>
      <div>
        {personsLoaded && console.log(persons)}
        {personsLoaded &&
          persons.map((p) => (
            <div key={p.createdAt}>
              {p.name}
              {p.email}
            </div>
          ))}
      </div>
    </div>
  );
};

export default App;
